# Pactflow - Getting started with NodeJS
This repository is an example on how to create a basic pactflow in NodeJS. This is made possible by taking the tutorial from katacoda.com.

### What is Contract Testing?
Contract testing is a technique for testing software application interfaces and integrations.

### Definitions
- Consumer: An application that makes use of the functionality or data from another application to do its job. Think of consumer as the Client Side.
- Provider: An application that provides functionality or data for other applications to use, often via an API. Think of provider as the Service Side.

### What is a Contract?
A contract is made when the consumer and provider made a pact.

### What is a Pact?
A pact is a collection of interactions which describe how the consumer expectes the provider to behave. Think of this as making an HTTP request to a service. And your expecting the Service to response in a certain way.

### Installation
To install all dependecies please do `npm install`.

### Requirement
You need to create your own `Pactflow` account. By following this link and clicking the Try for Free. https://pactflow.io/
<br>
Then you need to click the gear icon(settings) and click `Copy Env Vars`.
<br>
In your terminal please paste it and press enter. And please update the values with your own credentials.
```
export PACT_BROKER_BASE_URL=https:<YOUR_URL_HERE>
export PACT_BROKER_TOKEN=<YOUR_TOKEN_HERE>
```
### Scripts Command
`npm run test:consumer` it should run the consumer tests. <br>
`npm test:provider` it should run the provider tests. <br>
`npm run publish` it should publish your tests in your pacflow account.